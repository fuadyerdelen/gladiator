import java.awt.*;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class DancingVonGladiators extends JFrame {

    //Methode für die PopUp-Box
    public static void infoBox(String infoMessage, String titleBar) {
        JOptionPane.showMessageDialog(null, infoMessage, "Gut zu wissen!  " + titleBar, JOptionPane.INFORMATION_MESSAGE);
    }

    private String getGladiators() {
        JFileChooser chooser = new JFileChooser("C:\\Users\\mfuad\\Desktop\\gladiator");
        chooser.showOpenDialog(null);
        java.io.File file = chooser.getSelectedFile();
        String filename = file.getAbsolutePath();
        return filename;

    }


    public DancingVonGladiators() {

        JFrame frame = new JFrame();
        frame.setLayout(new BorderLayout());
        frame.setTitle("Gladiators Arena");


        JPanel panelSüd = new JPanel(new GridLayout(1, 1, 0, 1));
        frame.add(panelSüd, BorderLayout.SOUTH);


        JTextArea tabelle = new JTextArea();
        JScrollPane scrollMehr = new JScrollPane(tabelle);
        tabelle.setEditable(false);
        KampfStatTabelle kampfStatTabelle = new KampfStatTabelle();
        tabelle.append(kampfStatTabelle.bringtMirGladiators("gladiators.txt"));
        frame.add(tabelle, BorderLayout.NORTH);


        //SPEICHERN
        JButton sichereTabButton = new JButton("Speichern");
        sichereTabButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tabelle.selectAll();
                tabelle.replaceSelection("");
                kampfStatTabelle.speichernGladiators();
                tabelle.append(kampfStatTabelle.bringtMirGladiators("gladiators.txt"));
                infoBox("Tabelle wurde gespeichert", "Speichern");
            }
        });

        //RESET
        JButton resetTabButton = new JButton("Reset");
        resetTabButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                KampfStatTabelle.resetGladiators();
                tabelle.selectAll();
                tabelle.replaceSelection("");
                tabelle.append(KampfStatTabelle.bringtMirGladiators("gladiators.txt"));
                infoBox("Tabelle wurde zurückgesetzt", "Reset");
                //tabelle.append(kampfStatTabelle.bringtMirGladiators("gladiators.txt"));
            }
        });

        //LADEN
        JButton ladeTabButton1 = new JButton("Laden");
        ladeTabButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tabelle.selectAll();
                tabelle.replaceSelection("");
                tabelle.append(kampfStatTabelle.bringtMirGladiators(getGladiators()));

            }
        });

        //ADD GLADIATOR
        JButton neuerGladiatorButton = new JButton("Add Gladiator");
        neuerGladiatorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String gladName = JOptionPane.showInputDialog("Gib den Namen des Gladiators ein: ");
                kampfStatTabelle.addGladiator(new GladStat(gladName, 0, 0, 0, false));
                tabelle.selectAll();
                tabelle.replaceSelection("");
                KampfStatTabelle.sortGladiators();
                kampfStatTabelle.speichernGladiators();
                tabelle.append(kampfStatTabelle.bringtMirGladiators("gladiators.txt"));

            }
        });

        //REMOVE GLADIATOR
        JButton löscheGladiatorButton = new JButton("Remove Gladiator");
        löscheGladiatorButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String gladName = JOptionPane.showInputDialog("Gib den Namen des Gladiators ein: ");
                KampfStatTabelle.removeGladiator(gladName);
                int result = JOptionPane.showConfirmDialog(frame, "Bist du dich sicher?", "Fire the Gladiator !",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);

                if (result == JOptionPane.YES_OPTION) {
                    KampfStatTabelle.sortGladiators();
                    KampfStatTabelle.speichernGladiators();
                    tabelle.selectAll();
                    tabelle.replaceSelection("");
                    tabelle.append(KampfStatTabelle.bringtMirGladiators("gladiators.txt"));
                }

            }
        });

        //Neuer Kampf
        JButton neuerKampfButton = new JButton("Neuer Kampf");
        neuerKampfButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JComboBox<String> gladiators = new JComboBox<String>();
                for (int i = 0; i < KampfStatTabelle.getArrayLength(); i++) {
                    if (kampfStatTabelle.getGladiator(i) != null && !kampfStatTabelle.getGladiator(i).getIsTot()) {
                        gladiators.addItem(kampfStatTabelle.getGladiator(i).getName());

                    }
                }
                JComboBox<String> gladiators2 = new JComboBox<String>();
                for (int i = 0; i < KampfStatTabelle.getArrayLength(); i++) {
                    if (kampfStatTabelle.getGladiator(i) != null && !kampfStatTabelle.getGladiator(i).getIsTot()) {
                        gladiators2.addItem(kampfStatTabelle.getGladiator(i).getName());

                    }
                }
                JPanel panel = new JPanel();
                panel.add(gladiators);
                panel.add(gladiators2);
                JOptionPane.showMessageDialog(null, panel);
                String selectedName1 = (String) gladiators.getSelectedItem();
                String selectedName2 = (String) gladiators2.getSelectedItem();
                GladStat gladiator1 = kampfStatTabelle.getGladiatorByName(selectedName1);
                GladStat gladiator2 = kampfStatTabelle.getGladiatorByName(selectedName2);

                if (gladiator1 == gladiator2) {
                    infoBox("Gladiators können nicht gegeneinander kämpfen", "Fehler");

                }

                KampfStatTabelle.kampfStart(gladiator1, gladiator2);
                tabelle.selectAll();
                tabelle.replaceSelection("");
                tabelle.append(KampfStatTabelle.bringtMirGladiators("gladiators.txt"));


                //KampfStatTabelle.kampfStart(gladiator1, gladiator2);

            }
        });


        panelSüd.add(sichereTabButton);
        panelSüd.add(resetTabButton);
        panelSüd.add(ladeTabButton1);
        panelSüd.add(neuerGladiatorButton);
        panelSüd.add(löscheGladiatorButton);
        panelSüd.add(neuerKampfButton);

        frame.setSize(700, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }


    public static void main(String[] args) {


        KampfStatTabelle.addNamen();
        KampfStatTabelle.addGladiator(new GladStat("Maximus", 0, 0, 0, false));
        KampfStatTabelle.addGladiator(new GladStat("Salim", 0, 0, 0, false));
        KampfStatTabelle.addGladiator(new GladStat("Sparta", 0, 0, 0, false));
        KampfStatTabelle.addGladiator(new GladStat("Fuad", 0, 0, 0, false));

        //KampfStatTabelle.kampfStart(KampfStatTabelle.getGladiators()[(int) (Math.random() * KampfStatTabelle.getArrayLength())], KampfStatTabelle.getGladiators()[(int) (Math.random() * KampfStatTabelle.getArrayLength())]);
        //KampfStatTabelle.kampfStart(KampfStatTabelle.getGladiators()[(int) (Math.random() * KampfStatTabelle.getArrayLength())], KampfStatTabelle.getGladiators()[(int) (Math.random() * KampfStatTabelle.getArrayLength())]);

        new DancingVonGladiators();

    }
}
