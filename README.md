Programmieraufgabe für Vorlesung OOSE

Ziel:
Ziel ist es eine graphische Fensteranwendung zu schreiben in der für Gladiatorenspiele eine
Kampfstatistiktabelle (im folgenden einfach Tabelle genannt) mit den Gladiatoren, deren Siegen,
Niederlagen und Siegquoten dargestellt wird, die auch entsprechend der Siegquoten der Gladiatoren
sortiert ist (oben steht der erfolgreichste Gladiator unten der erfolgloseste).

Anforderungen:
Das fachliche Datenmodell:

REQ1: 
Schreiben Sie eine Klasse GladStat, die die Statistikdaten zu einem Gladiator repräsentiert. Ein
Gladiator in der Tabelle hat üblicherweise folgende Statistikdaten: Name, Anzahl gemachter Kämpfe,
Anzahl eigener Siege, Anzahl Niederlagen, Siegquote (Anzahl Siege/Niederl), und ob er noch lebendig
oder leider tot ist.
1. Die Klasse GladStat soll auch eine Methode int compareTo(GladStat gl) bereitstellen, die bei
der Sortierung aufgerufen wird, um die Statistikdaten von zwei Gladiatoren zu vergleichen.
2. Hinweis zur Sortierung in Tabellen: Gladiatoren mit der höheren Anzahl Siege landen oben.
Wenn zwei Gladiatoren die gleichen Anzahl Siege haben, entscheidet die bessere Siegquote.
Falls diese auch gleich sein sollte entscheidet, ob Gladiatoren noch lebendig sind oder tot sind.
Falls alles gleich ist, wird alphabetisch nach Namen sortiert.

REQ2: 
Schreiben Sie auch eine Klasse KampfStatTabelle, welche die Gladiatoren als Array von
GladStat-Objekten enthält und sie in geordneter Reihenfolge (zuerst kommt der erfolgreichste
Gladiator zuletzt der erfolgloseste) hält.
REQ3: Die Klasse KampfStatTabelle stellt auch folgende Funktionen bereit:
1. Hinzufügen eines neuen Gladiators in die Tabelle (neues GladStat-Objekt in der Tabelle). Es
kann das Array zunächst darauf ausgelegt werden, dass 20 Gladiatoren hinzugefügt werden
können. Beim Hinzufügen des 21. Gladiators wird dann die Arraygröße verdoppelt.
2. Finden eines Gladiators in der Tabelle
3. Alle Gladiatoren sortiert als Array bekommen
4. Löschen eines vorhandenen Gladiators aus der Tabelle.
5. Reset: Es werden allen vorhandenen Gladiatoren alle Werte zurückgesetzt bis auf den
Namen (auch der Status tod wird wieder auf lebendig gesetzt).
6. Eine Methode in der das Kampfergebnis zwischen zwei Gladiatoren
berechnet1 und in den Gladiatoren abgelegt wird. Hierzu müssen der Methode die beiden
Gladiatoren, der jeweilige Sieger und jeweils ob ein Gladiator gestorben ist übergeben
werden. Daraus werden dann für die beiden Gladiatoren die Werte (Siege, Niederlagen, …)
verändert und anschließend die Tabelle neu sortiert.
7. Eine Methode in der ein Pfad zu einer Datei übergeben wird und die kompletten Daten der
KampfStatTabelle in der Datei gespeichert werden.
8. Eine Methode in der ein Pfad zu einer Datei übergeben wird und eine KampfStatTabelle mit
den Daten aus der Datei geladen wird.

Anforderungen an das Graphische User Interface (GUI):

REQ4: 
Schreiben Sie ein Hauptfenster in dem die Tabelle2 dargestellt wird. Das Hauptfenster soll auch
folgende Buttons anbieten:
1. Neue Tab: Löscht die vorher evtl. geöffnete Tabelle und fängt mit einem neuen
Tabellenobjekt an.
2. Reset Tab: Löscht die Ergebnisse behält jedoch die Gladiatoren bei.
3. Lade Tab: Lädt eine Tabelle aus einer Datei (vom User per Dateidialog wählbar).
4. Sichere Tab: Speichert die Tabelle in eine Datei (vom User per Dateidialog wählbar).
5. Neuer Gladiator: Öffnet einen Dialog mit dem ein neuer Gladiator in der Tabelle eingetragen
werden kann.
6. Lösche Gladiator: Öffnet einen Dialog mit dem ein Gladiator ausgewählt wird, der dann
gelöscht wird (Zwischenfrage: Sind Sie sicher, dass Sie Gladiator … löschen möchten).
7. Neuer Kampf: Öffnet einen „Neuer Kampf“-Dialog mit dem man zwei noch lebende
Gladiatoren z.B. per Combobox aus den vorhandenen auswählen kann (Vorsicht: Ein
Gladiator soll nur einmal ausgewählt werden!). Anschließend soll es möglich sein in dem
Dialog einen Button „Starte Kampf“ zu klicken. Hier können Sie den Code von Aufgabenblatt
6 einbauen und den Kampf simulieren und anschließend im Hauptfenster eine JTextArea mit
der Kampfzusammenfassung (Ausgabe des Kampfes) anzeigen und gleichzeitig dann die Ergebnisse
in die Kampfstatistiktabelle übertragen3.
Falls Sie in der früheren Übung die Gladiatorenaufgabe von Aufgabenblatt 06 nicht ganz
geschafft haben, können Sie alternativ den Dialog so umgestalten, dass man dort die Ergebnisse
eines Kampfes (also, Sieg, Niederlage und ob ein Gladiator gestorben ist) eintragen
kann.
