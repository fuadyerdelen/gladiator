import javax.swing.*;
import java.io.*;
import java.util.stream.Collectors;

public class KampfStatTabelle {

    // ich habe die Variable und Methode "static" gemacht, weil ich sie in der main Methode benutzen kann
    public static GladStat[] gladiators = new GladStat[20];

    public void setGladiators(GladStat[] gladiators) {
        this.gladiators = gladiators;
    }

    public static GladStat[] getGladiators() {
        return gladiators;
    }

    private static String[] gladiatorenNamen = {"Sparta", "Commod", "Marcus", "Flamma", "Crixus", "Verus", "Priscus", "Hermes", "Secutor", "Murmil", "Thraex", "Myrmil", "Andabat"};

    public static void addNamen() {
        for (int i = 0; i < gladiatorenNamen.length; i++) {
            gladiators[i] = new GladStat(gladiatorenNamen[i], 0, 0, 0, false);
        }
    }


    private static GladStat findGladiator(String name) {
        for (GladStat gladiator : gladiators) {
            if (gladiator != null && gladiator.getName().equals(name)) {
                return gladiator;
            }
        }
        return null;// ---> Wenn es nicht gefunden wurde, dann gebe ich null zurück
    }

    public static void addGladiator(GladStat neueGladiator) {
        for (int i = 0; i < gladiators.length; i++) {
            if (gladiators[i] == null) {
                gladiators[i] = neueGladiator;
                sortGladiators();
                speichernGladiators();
                break;
                // wenn gladStat[i] nicht null ist und i ist gleich (der Länge des Arrays - 1)
                // ist, heißt das, dass das Array voll ist
            } else if ((i == gladiators.length - 1) && !(gladiators[i] == null)) {
                verdoppleArray(); // --> die Länge des Arrays verdoppeln
                //wenn es verdoppelt wurde, dann füge ich den neuen Gladiator hinzu
                gladiators[gladiators.length] = neueGladiator;
            }
        }
        // Nachdem ich neu Gladiator hinzugefügt habe, muss ich die Gladiatoren nochmal sortieren


    }

    public static void removeGladiator(String name) {
        for (int i = 0; i < gladiators.length; i++) {

            if (gladiators[i] != null && gladiators[i].getName().equals(name)) {
                gladiators[i] = null;
                break;
            }
        }
        // Nachdem ich Gladiator gelöscht habe, muss ich die Gladiatoren nochmal
        // sortieren
        sortGladiators();
    }

    public static void resetGladiators() {
        // hier setze ich alle Werte der Gladiatoren auf 0 und false
        for (int i = 0; i < gladiators.length -1; i++) {
            if (gladiators[i] != null) {
                // hier setze ich alle Werte der Gladiatoren auf 0 und false
                /*
                * Ich habe den aktuellen Wert subtrahiert, um die Werte auf 0 abzugleichen
                 * */
                gladiators[i].setGesamteKampf(-gladiators[i].getGesamteKampf());
                gladiators[i].setGesamteSiege(-gladiators[i].getGesamteSiege());
                gladiators[i].setSiegquote(0.0);
                gladiators[i].setNiederlagen(-gladiators[i].getNiederlagen());
                gladiators[i].setIsTot(false);

            }
        }
        // dann muss ich die Gladiatoren nochmal sortieren(Nach Namen)
        sortGladiators();
        // und dann speichere ich die Gladiatoren in der Datei
        speichernGladiators();
        System.out.println("Gladiatoren wurden zurückgesetzt!");
    }

    private static void verdoppleArray() {

        GladStat[] neuFeld = new GladStat[gladiators.length * 2];

        for (int i = 0; i < gladiators.length; i++) {
            neuFeld[i] = gladiators[i];
        }
        gladiators = neuFeld;
    }

    public static void sortGladiators() {

        /*
         * Hier sortiere ich die Gladiatoren nach den folgenden Kriterien:
         * 1. Gesamte Siege
         * 2. Siegquote
         * 3. IsTot
         * 4. Name
         */

        for (int i = 0; i < gladiators.length; i++) {

            for (int j = 0; j < gladiators.length - i - 1; j++) {

                if (gladiators[j] != null && gladiators[j + 1] != null) {

                    if (gladiators[j].getGesamteSiege() < gladiators[j + 1].getGesamteSiege()) {
                        GladStat temp = gladiators[j];
                        gladiators[j] = gladiators[j + 1];
                        gladiators[j + 1] = temp;
                    } else if (gladiators[j].getGesamteSiege() == gladiators[j + 1].getGesamteSiege()) {
                        if (gladiators[j].getSiegquote() < gladiators[j + 1].getSiegquote()) {
                            GladStat temp = gladiators[j];
                            gladiators[j] = gladiators[j + 1];
                            gladiators[j + 1] = temp;

                        } else if (gladiators[j].getSiegquote() == gladiators[j + 1].getSiegquote()) {
                            if ((gladiators[j].getIsTot() ? 1 : 0) < (gladiators[j + 1].getIsTot() ? 1 : 0)) {
                                GladStat temp = gladiators[j];
                                gladiators[j] = gladiators[j + 1];
                                gladiators[j + 1] = temp;

                            } else if (gladiators[j].getIsTot() == gladiators[j + 1].getIsTot()) {
                                if (gladiators[j].getName().compareTo(gladiators[j + 1].getName()) > 0) {
                                    GladStat temp = gladiators[j];
                                    gladiators[j] = gladiators[j + 1];
                                    gladiators[j + 1] = temp;

                                }
                            }
                        }
                    }
                }
            }
        }
    }


    //Tabelle
    public static String makeTabelleGreatAgain() {
        String tabelle = "Name\tGesamteKampf\tSiege\tNiederlagen\t\tSiegquote\tIsTot\n";
        for (GladStat gladiator : gladiators) {
            sortGladiators();
            if (gladiator != null) {
                tabelle += gladiator.getName() + "\t" + "\t" + gladiator.getGesamteKampf() + "\t" + "\t" + "\t" + gladiator.getGesamteSiege() + "\t" + "\t" + "\t" + gladiator.getNiederlagen() + "\t" + "\t" + "\t" + gladiator.getSiegquote() + "\t" + "\t" + "\t" + gladiator.getIsTot() + "\n";

            }
        }
        sortGladiators();
        return tabelle;
    }


    private static int arrayLength = gladiators.length - 1;

    public void setArrayLength(int arrayLength) {
        this.arrayLength = arrayLength;
    }

    public static int getArrayLength() {
        return arrayLength;
    }


    public static void kampfStart(GladStat gladiator1, GladStat gladiator2) {

        if (gladiator1 == null || gladiator2 == null || gladiator1.getName().equals(gladiator2.getName()) || gladiator1.getIsTot() || gladiator2.getIsTot()) {
            while (true) {
                gladiator1 = gladiators[(int) (Math.random() * (gladiators.length - 1))];
                gladiator2 = gladiators[(int) (Math.random() * (gladiators.length - 1))];
                if (gladiator1 != null && gladiator2 != null && gladiator1.getName() != gladiator2.getName() && !gladiator1.getIsTot() && !gladiator2.getIsTot()) {
                    break;
                }
            }
        }

        System.out.println("Kampf zwischen " + gladiator1.getName() + " und " + gladiator2.getName() + " beginnt!");
        gladiator1.setGesamteKampf(1);
        gladiator2.setGesamteKampf(1);
        GladStat[] kampfinderGladiators = {gladiator1, gladiator2};

        int win = (int) Math.random() * 2;
        int lose = win == 1 ? 0 : 1;

        System.out.println(kampfinderGladiators[win].getName() + " hat gewonnen");


        kampfinderGladiators[win].setGesamteSiege(1);

        kampfinderGladiators[lose].setNiederlagen(1);
        kampfinderGladiators[lose].setIsTot(true);

        speichernGladiators();
        bringtMirGladiators("gladiators.txt");
    }


    //Datei erstellen
    public static void speichernGladiators() {
        try {
            File file = new File("gladiators.txt");
            if (!file.exists()) {
                file.createNewFile();
            }

            BufferedWriter meineGladiators = new BufferedWriter(new FileWriter(file));
            meineGladiators.write("Name\tGesamteKampf\tSiege\tNiederlagen\tSiegquote\tIsTot\n");
            sortGladiators();
            for (GladStat gladiator : gladiators) {
                if (gladiator != null) {
                    meineGladiators.write(gladiator.getName() + "\t");
                    meineGladiators.write(gladiator.getGesamteKampf() + "\t");
                    meineGladiators.write(gladiator.getGesamteSiege() + "\t");
                    meineGladiators.write(gladiator.getNiederlagen() + "\t");
                    meineGladiators.write(Double.toString(gladiator.getSiegquote()) + "\t");
                    meineGladiators.write(gladiator.getIsTot() + "\n");

                }
            }

            meineGladiators.close();
            System.out.println("Gladiators erfolgreich geschrieben!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String bringtMirGladiators(String fileName) {
        try {
            BufferedReader meineGladiators = new BufferedReader(new FileReader(fileName));
            /*
                String zeile = meineGladiators.readLine();
                while (zeile != null) {
                    System.out.println(zeile);
                    zeile = meineGladiators.readLine();
                }
            */

            String gesamteText = meineGladiators.readLine();
            /*
                Hier nutze ich die Methode lines(), um alle Zeilen in der Datei zu lesen
                und dann nutze ich die Methode collect(), um die Zeilen in einem String zu speichern
                und dann nutze ich die Methode joining("\n"), um die Zeilen mit einem Zeilenumbruch zu trennen
                und dann speichere ich den String in der Variable gesamteText
            */
            gesamteText = meineGladiators.lines().collect(Collectors.joining("\n"));
            meineGladiators.close();
            return gesamteText; // ---> Hier gebe ich den gesamten Gladiator zurück

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }


    public GladStat getGladiator(int index) {
        return gladiators[index];
    }

    public GladStat getGladiatorByName(String name) {

        for (GladStat gladiator : gladiators) {
            if (gladiator.getName().equals(name)) {
                return gladiator;
            }
        }
        return null;
    }

    public static void main(String[] args) {

        addNamen();
        addGladiator(new GladStat("Maximus", 0, 0, 0, false));
        addGladiator(new GladStat("Salim", 0, 0, 0, false));
        addGladiator(new GladStat("Sparta", 0, 0, 0, false));
        addGladiator(new GladStat("Fuad", 0, 0, 0, false));

        //removeGladiator("Maximus");



        kampfStart(gladiators[(int) (Math.random() * arrayLength)], gladiators[(int) (Math.random() * arrayLength)]);
        kampfStart(gladiators[(int) (Math.random() * arrayLength)], gladiators[(int) (Math.random() * arrayLength)]);


        System.out.println();
        // System.out.println(makeTabelleGreatAgain());

        //speichernGladiators();

        System.out.println(bringtMirGladiators("gladiators.txt"));
        //System.out.print(findGladiator("Fuad").getName());
        //reset();
        //System.out.println(bringtMirGladiators("gladiators.txt"));


    }

}

