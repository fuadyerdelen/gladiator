import java.text.DecimalFormat;

public class GladStat {

    private String name;
    private int gesamteKampf;
    private int gesamteSiege;
    private int gesamteNiederlagen;
    private double siegquote = rechnerSiegquote();
    private boolean isTot;

    private double rechnerSiegquote() {
        if (gesamteNiederlagen == 0) {
            return gesamteSiege;
        } else {
            double siegquote = ((double) gesamteSiege) / gesamteNiederlagen;
            //Hier wird die Siegquote auf eine Nachkommastelle gerundet, um die Sortierung zu vereinfachen
            return Double.parseDouble(String.format("%.1f", siegquote).replace(",", "."));
        }
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setGesamteKampf(int gesamteKampf) {
        this.gesamteKampf += gesamteKampf;
    }

    public int getGesamteKampf() {
        return gesamteKampf;
    }

    public void setGesamteSiege(int gesamteSiege) {
        this.gesamteSiege += gesamteSiege;
    }

    public int getGesamteSiege() {
        return gesamteSiege;
    }

    public void setSiegquote(double siegquote) {
        this.siegquote = siegquote;
    }

    public double getSiegquote() {
        return rechnerSiegquote();
    }

    public void setNiederlagen(int gesamteNiederlagen) {
        this.gesamteNiederlagen += gesamteNiederlagen;
    }

    public int getNiederlagen() {
        return gesamteNiederlagen;
    }

    public void setIsTot(boolean isTot) {
        this.isTot = isTot;
    }

    public boolean getIsTot() {
        return isTot;
    }

    public GladStat(String name, int gesamteKampf, int gesamteSiege, int gesamteNiederlagen,
                    boolean isTot) {
        this.name = name;
        this.gesamteKampf = gesamteKampf;
        this.gesamteSiege = gesamteSiege;
        this.gesamteNiederlagen = gesamteNiederlagen;
        // this.siegquote = siegquote;
        this.siegquote = rechnerSiegquote();
        this.isTot = isTot;
    }

    public int compareTo(GladStat gl) {

        int compareMitSiege = Integer.compare(gl.gesamteSiege, this.gesamteSiege);
        int compareMitSiegquote = Integer.compare(gl.gesamteSiege, this.gesamteSiege);
        int compareMitIstot = Integer.compare(gl.gesamteSiege, this.gesamteSiege);

        if (compareMitSiege != 0) {
            return compareMitSiege;
        } else if (compareMitSiegquote != 0) {
            return compareMitSiegquote;
        } else if (compareMitIstot != 0) {
            return compareMitIstot;
        }
        return this.name.compareTo(gl.name);

    }

    public static void main(String[] args) {

        // GladStat gladiator_1 = new GladStat("Maximus", 14, 12, 0, 12.0, false);
        // GladStat gladiator_2 = new GladStat("Salim", 14, 12, 0, 12.0, false);

        /*
         * System.out.println(gladiator_1.compareTo(gladiator_2));
         *
         * if (gladiator_1.compareTo(gladiator_2) < 0) {
         * System.out.println(gladiator_1.name + gladiator_2.name);
         * } else if (gladiator_1.compareTo(gladiator_2) > 0) {
         * System.out.println(gladiator_2.name + gladiator_1.name);
         * }
         */
    }
}

